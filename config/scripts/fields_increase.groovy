/**
 * fields value increase script
 *
 * @author  chenxin<chenxin619315@gmail.com>
 * @date    2016-05-03
*/

/*
 * @param   act
 * @param   fields array
 * @param   value
*/
if ( act == 'increase' ) {
    def i = 0;
    for ( field in fields ) {
        ctx._source[field] += values[i++]
    }
} else if ( act == 'reduce' ) {
    def i = 0;
    for ( field in fields ) {
        ctx._source[field] -= values[i++]
    }
}
